using Microsoft.EntityFrameworkCore;
using PruebaRedar.DBService;
using PruebaRedar.Services;
using PruebaRedar.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<ContextDB>(opt =>
    opt.UseInMemoryDatabase("Redar"));

builder.Services.AddScoped<ICandidate, CandidateService>();
builder.Services.AddScoped<IExperience, ExperienceService>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Candidates}/{action=Index}/{id?}");

app.Run();
