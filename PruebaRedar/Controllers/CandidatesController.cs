﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PruebaRedar.DBService;
using PruebaRedar.Models;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;

namespace PruebaRedar.Controllers
{
    public class CandidatesController : Controller
    {
        private readonly ICandidate _canditateService;
        private readonly IExperience _experienceService;

        public CandidatesController(ICandidate candidateContext, IExperience experienceContext)
        {
            _canditateService = candidateContext;
            _experienceService = experienceContext;
        }

        // GET: Candidates
        public async Task<IActionResult> Index()
        {
            var candidates = await _canditateService.GetAllCandidates();
            return candidates is not null ? 
                        View(candidates) :
                        Problem("Entity set 'ContextDB.Candidates' is null.");
        }

        // GET: Candidates/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Candidates/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Surname,Email,BirthDate")] Candidate candidate)
        {
            if (ModelState.IsValid)
            {
                candidate.InsertDate = DateTime.Now;
                var candidates = await _canditateService.GetAllCandidates();
                if (candidates.Any(c => c.Email == candidate.Email))
                    return Conflict("email must be unique");

                await _canditateService.PostCandidate(candidate);
                return RedirectToAction(nameof(Index));
            }
            return View(candidate);
        }

        // GET: Candidates/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id is null)
                return NotFound();

            var candidate = await _canditateService.GetCandidate(id.Value);
            if (candidate is null)
                return NotFound();

            return View(candidate);
        }

        // POST: Candidates/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Surname,BirthDate,Email,InsertDate")] Candidate candidate)
        {
            if (id != candidate.Id)
                return NotFound();

            try
            {
                candidate.ModifyDate = DateTime.Now;
                await _canditateService.PutCandidate(candidate);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Candidates/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            var candidate = await _canditateService.GetCandidate(id);
            if (candidate is null)
                return NotFound();

            return View(candidate);
        }

        // POST: Candidates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidate = await _canditateService.GetCandidate(id);
            if (candidate is null)
                return NotFound();

            var experiences = await _experienceService.GetAllExperiencesByCandidateId(id);
            if (experiences.Any())
                foreach(var i in experiences)
                      _experienceService.DeleteExperience(i);

            await _canditateService.DeleteCandidate(candidate);
            return RedirectToAction(nameof(Index));
        }
    }
}
