﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PruebaRedar.DBService;
using PruebaRedar.Models;

namespace PruebaRedar.Controllers
{
    public class ExperiencesController : Controller
    {
        private readonly ICandidate _canditateService;
        private readonly IExperience _experienceService;

        public ExperiencesController(ICandidate candidateContext, IExperience experienceContext)
        {
            _canditateService = candidateContext;
            _experienceService = experienceContext;
        }

        // GET: Experiences
        public async Task<IActionResult> Index()
        {
            var experiences = await _experienceService.GetAllExperiences();
            return experiences is not null ?
                        View(experiences) :
                        Problem("Entity set 'ContextDB.Experiences' is null.");
        }

        // GET: Experiences/Create
        public async Task<IActionResult> Create()
        {
            var candidates = await _canditateService.GetAllCandidates();
            ViewData["CandidateId"] = new SelectList(candidates, "Id", "Id");
            return View();
        }

        // POST: Experiences/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CandidateId,Company,Job,Description,Salary,BeginDate,EndDate")] Experience experience)
        {
            experience.InsertDate = DateTime.Now;
            await _experienceService.PostExperience(experience);
            return RedirectToAction(nameof(Index));
        }

        // GET: Experiences/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id is null)
                return NotFound();

            var experience = await _experienceService.GetExperience(id.Value);
            if (experience is null)
                return NotFound();

            return View(experience);
        }

        // POST: Experiences/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CandidateId,Company,Job,Description,Salary,BeginDate,EndDate,InsertDate")] Experience experience)
        {
            if (id != experience.Id)
                return NotFound();

            try
            {
                experience.ModifyDate = DateTime.Now;
                await _experienceService.PutExperience(experience);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
