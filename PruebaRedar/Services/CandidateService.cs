﻿using Microsoft.AspNetCore.Routing.Matching;
using Microsoft.EntityFrameworkCore;
using NuGet.Versioning;
using PruebaRedar.DBService;
using PruebaRedar.Models;

namespace PruebaRedar.Services
{
    public class CandidateService : ContextDB, ICandidate
    {
        public CandidateService(DbContextOptions<ContextDB> options) : base(options)
        {
        }

        public async Task<bool> DeleteCandidate(Candidate candidate)
        {
            var result = Candidates.Remove(candidate);
            await SaveChangesAsync();
            
            return result.Entity.Id < 1 ? throw new ApplicationException("the record has not been deleted in the db") : true;
        }

        public async Task<IEnumerable<Candidate>> GetAllCandidates() => await Candidates.ToListAsync();

        public async Task<Candidate?> GetCandidate(int id) => await Candidates.FindAsync(id);

        public async Task<Candidate?> GetCandidateByEmail(string email) => await Candidates.FirstOrDefaultAsync(c => c.Email == email);

        public async Task<int> PostCandidate(Candidate candidate)
        {
            var result = Candidates.Add(candidate);
            await SaveChangesAsync();

            return result.Entity.Id < 1 ? throw new ApplicationException("the record has not been inserted in the db") : result.Entity.Id;
        }

        public async Task<int> PutCandidate(Candidate candidate)
        {
            var result = Candidates.Update(candidate);
            await SaveChangesAsync();
            
            return result.Entity.ModifyDate != candidate.ModifyDate ? throw new ApplicationException("the record has not been updated in the db") : result.Entity.Id;
        }
    }
}
