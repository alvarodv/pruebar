﻿
using PruebaRedar.Models;

namespace PruebaRedar.DBService
{
    public interface IExperience
    {
        Task<IEnumerable<Experience>> GetAllExperiences();
        Task<IEnumerable<Experience>> GetAllExperiencesByCandidateId(int candidateId);
        Task<int> PostExperience(Experience candidate);
        Task<int> PutExperience(Experience candidate);
        Task<bool> DeleteExperience(Experience candidate);
        Task<Experience?> GetExperience(int id);

    }
}
