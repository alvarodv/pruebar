﻿using Microsoft.Extensions.Hosting;
using PruebaRedar.Models;

namespace PruebaRedar.DBService
{
    public interface ICandidate
    {
        Task<IEnumerable<Candidate>> GetAllCandidates();
        Task<Candidate?> GetCandidate(int id);
        Task<Candidate?> GetCandidateByEmail(string email);
        Task<int> PostCandidate(Candidate candidate);
        Task<int> PutCandidate(Candidate candidate);
        Task<bool> DeleteCandidate(Candidate candidate);
    }
}
