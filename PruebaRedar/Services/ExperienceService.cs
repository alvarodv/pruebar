﻿using Microsoft.AspNetCore.Routing.Matching;
using Microsoft.EntityFrameworkCore;
using PruebaRedar.DBService;
using PruebaRedar.Models;


namespace PruebaRedar.Services
{
    public class ExperienceService : ContextDB, IExperience
    {
        public ExperienceService(DbContextOptions<ContextDB> options) : base(options)
        {
        }
        public async Task<IEnumerable<Experience>> GetAllExperiences() => await Experiences.ToListAsync();

        public async Task<IEnumerable<Experience>> GetAllExperiencesByCandidateId(int candidateId) => await Experiences.Where(c=> c.CandidateId == candidateId).ToListAsync();

        public async Task<int> PostExperience(Experience experience)
        {
            var result = Experiences.Add(experience);
            await SaveChangesAsync();

            return result.Entity.Id < 1 ? throw new ApplicationException("the experience has not been inserted in the db") : result.Entity.Id;
        }
        public async Task<bool> DeleteExperience(Experience experience)
        {
            var result = Experiences.Remove(experience);
            await SaveChangesAsync();

            return result.Entity.Id < 1 ? throw new ApplicationException("the experience has not been deleted in the db") : true;
        }

        public async Task<int> PutExperience(Experience experience)
        {
            var result = Experiences.Update(experience);
            await SaveChangesAsync();

            return result.Entity.ModifyDate != experience.ModifyDate ? throw new ApplicationException("the experience has not been updated in the db") : result.Entity.Id;
        }

        public async Task<Experience?> GetExperience(int id) => await Experiences.FindAsync(id);
    }
}
