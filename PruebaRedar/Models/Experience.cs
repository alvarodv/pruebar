﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace PruebaRedar.Models
{
    public class Experience
    {
        [Key]
        public int Id { get; set; }
        public int CandidateId { get; set; }

        [MaxLength(100)]
        public string Company { get; set; }

        [MaxLength(100)]
        public string Job { get; set; }

        [MaxLength(4000)]
        public string Description { get; set; }
        public double Salary { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public Experience Candidate { get; set; }
    }
}
