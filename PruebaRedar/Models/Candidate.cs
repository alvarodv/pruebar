﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;


namespace PruebaRedar.Models
{
    public class Candidate
    {
        [Key]
        public int Id { get; set; }
     
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Surname { get; set; }

        [MaxLength(250)]
        public string Email { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
